function funcLoad(user, app)
  vars = {}
  vars.users = {}
  for k,v in pairs(User.getList()) do
    if not v.null then
      if v.protected then
        table.insert(vars.users, {name = v:getName(), protected = "", x = 0})
      else
        table.insert(vars.users, {name = v:getName(), protected = nil, x = 0})
      end
    end
  end
  vars.camera = 0
  vars.helpText = "To begin, click your username."
  vars.clickprompt = "Click again to log in."
  vars.opened = 0

  tile = {available = false}

  function vars.login(vars)
    User.setCurrent(User.getIndex(vars.users[vars.opened].name))
    vars.opened = 0
    vars.camera = 0
    vars.helpText = "To begin, click your username."
    for k,v in pairs(vars.users) do
      v.x = 0
      v.protected = ""
    end
  end
  
  return vars,tile,{}
end

function funcOpen(vars, user, app)
end

function funcIdle(vars, vars2, user, user2, app, app2)
  if user == user2 and app == app2 then
    for k,v in pairs(vars.users) do
      if vars.opened ~= k then
        if v.x > 0 then
          v.x = v.x - 1
        elseif v.x < 0 then
          v.x = v.x + 1
        end
      elseif v.protected then
        if v.x > math.max(10,string.len(v.protected)+2) then
          v.x = v.x - 1
        elseif v.x < math.max(10,string.len(v.protected)+2) then
          v.x = v.x + 1
        end
      else
        if v.x > string.len(vars.clickprompt)+1 then
          v.x = v.x - 1
        elseif v.x < string.len(vars.clickprompt)+1 then
          v.x = v.x + 1
        end
      end
    end
  end
end

function funcEvent(vars, user, app, event)
  if event[1] == "mouse_click" then
    for k,v in pairs(vars.users) do
      if event[4] >= (k*4)+vars.camera and event[4] <= (k*4)+2+vars.camera and event[3] >= 2 and event[3] <= v.name:len() + v.x + 4 then
        if vars.opened == k then
          if v.protected then
            vars.helpText = "To begin, click your username."
            vars.opened = 0
          else
            vars.login(vars)
          end
        else
          vars.opened = k
          if v.protected then
            vars.helpText = "Type in your password and press Enter."
          else
            vars.helpText = "Click again to log in."
          end
        end
      end
    end
  elseif event[1] == "mouse_scroll" then
    vars.camera = math.min(math.max(vars.camera + event[2], -(#vars.users*4)-4+Screen.height), 0)
  elseif event[1] == "char" then
    if vars.opened > 0 then
      vars.users[vars.opened].protected = string.sub(vars.users[vars.opened].protected .. event[2],1,Screen.width-vars.users[vars.opened].name:len()-7)
      vars.users[vars.opened].x = math.max(10,string.len(vars.users[vars.opened].protected)+2)
    end
  elseif event[1] == "key" then
    if event[2] == 14 and vars.opened > 0 then
      vars.users[vars.opened].protected = string.sub(vars.users[vars.opened].protected,1,string.len(vars.users[vars.opened].protected)-1)
    elseif event[2] == 28 and vars.opened > 0 then
      local realprotected = User.getFromIndex(User.getIndex(vars.users[vars.opened].name)):getAppData("Password")
      if fs.exists(realprotected) then
        realprotected = File.toString(realprotected)
        if vars.users[vars.opened].protected == realprotected then
          vars.login(vars)
        else
          vars.helpText = "Incorrect password, try again."
          vars.users[vars.opened].protected = ""
        end
      end
    end
  end
end

function funcDraw(vars, user, app)
  local function drawTile(name,protected,color,online,x,y,x2)
    if online then
      paintutils.drawLine(x,y,x,y+2,colors.lime)
    else
      paintutils.drawLine(x,y,x,y+2,colors.gray)
    end
  
    term.setBackgroundColor(color); term.setTextColor(colors.white)
  
    term.setCursorPos(x+1,y); term.write(string.rep(" ", name:len()+2))
    term.setCursorPos(x+1,y+1); term.write(string.rep(" ", name:len()+2))
    term.setCursorPos(x+2,y+1); term.write(name)
    term.setCursorPos(x+1,y+2); term.write(string.rep(" ", name:len()+2))
    
    for i=1,x2,1 do
      paintutils.drawLine(name:len() + 5,y,name:len() + 4 + x2,y,color)
      paintutils.drawLine(name:len() + 5,y+1,name:len() + 4 + x2,y+1,color)
      if x2 > 1 then
        if protected then
          paintutils.drawLine(name:len() + 5,y+1,name:len() + 3 + x2,y+1,colors.lightGray)
          term.setCursorPos(name:len() + 5,y+1); term.setBackgroundColor(colors.lightGray); term.setTextColor(colors.black)
          if frame <= 9 then
            term.write(string.rep("*",math.min(string.len(protected),x2-1)))
          elseif protected:len() >= Screen.width-name:len()-7 then
            term.write(string.rep("*",math.min(string.len(protected),x2-1)) .. "|")
          else
            term.write(string.rep("*",math.min(string.len(protected),x2-1)) .. "_")
          end
        else
          term.setCursorPos(name:len() + 5,y+1); term.setBackgroundColor(color); term.setTextColor(colors.black)
          term.write(string.sub(vars.clickprompt,1,x2-1))
        end
      end
      paintutils.drawLine(name:len() + 5,y+2,name:len() + 4 + x2,y+2,color)
    end
  end
  
  for k,v in pairs(vars.users) do
    drawTile(v.name,v.protected,colors.cyan,User.getFromName(v.name):isOnline(),2,k*4+vars.camera,v.x)
  end
  
  term.setBackgroundColor(colors.gray); term.setTextColor(colors.white)
  term.setCursorPos(1,2); term.clearLine()
  Draw.textCentered(vars.helpText,true)
end